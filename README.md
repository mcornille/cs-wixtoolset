# README #
### Why am i creating this? ###
Within the company i currently work for there are alot of internal developed applications.
Problem is that none of these devs have any MSI knowledge so to be able to get an msi instead of all there source files iam trying to creating a framework that can be used in Powershell or within there code
(Most of them use Powershell as there are automated tasks that need to be run before the msi is created.

the goal of this project:
* Easy general understandable functions
* Logic that the order of functions doenst matter meaning:
** They can add files first then msi information and the msi still builds correctly
* Logging, verbose logging, error handling
* Adding extra functions and features

### What can you do? ###

* I am a noob in programming so if you see errors or things that could be better let me know.
* Tips / Tricks or feature requests...
* Source is available so you can change, improve, add, ...

### What is this repository for? ###

* This repo is the sourcecode for a C# DLL that acts as interface between PowerShell and Wixsharp to create an MSI File

### How do I get set up? ###

* Dependencies
** Needs Wix Toolset http://wixtoolset.org/


### Contribution guidelines ###

* Based on [Wixsharp](https://wixsharp.codeplex.com/) by [Oleg_S](https://www.codeplex.com/site/users/view/oleg_s) ==> This project does all of the work the only thing my source does is create a framework around it.

### Who do I talk to? ###

* CS-Wix ==> Cornille Michiel (michiel@corit.be)