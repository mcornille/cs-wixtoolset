﻿#Import needed modules & DLL files
Import-Module "C:\Users\Michiel\OneDrive\Scripting\Powershell\Scripts\Securex VIE2000\3.0.1 - Development - Fix Duplicates\PS-WIX.3.0.1.psm1" -force
Add-Type -Path "D:\Development\WixToolset\Wix\Wix\bin\Debug\Wix.dll"
set-location C:\Temp

#Some variables
$SourceLocation = "C:\Users\Michiel\OneDrive\Scripting\Powershell\Scripts\Securex VIE2000\3.0.2 - Development - Wix DLL\_Source"
$WixToolsetLocation = "D:\Development\External\WixSharp.1.0.20.0\Wix_bin\bin"
$InstallDir = "[ProgramFilesFolder]Securex\VIE2000"

#Define a WIX Object
$MyWix = new-object Wix.Toolset

#Logging
$MyWix.logfilefullname = "C:\Temp\ErrorLog.log"
$MyWix.loggingon = $true
$MyWix.loglevel = 3 #Error = 1, Info = 2, Debug = 3

#Set Default Information
$MyWix.productname = "VIE2000"
$MyWix.productcode = [guid]::NewGuid()
$MyWix.upgradecode = [guid]::NewGuid()
$MyWix.companyname = "Securex"
$MyWix.productversion = "1.0.0"

#Add Files & folders
$MyWix.AddSourceFiles($SourceLocation)

#Add needed properties
$MyWix.AddProperty("REBOOT","ReallySuppress")
$MyWix.AddProperty("ALLUSERS","2")

#Add registry keys
$MyWix.AddHKLMRegValue("Software\Securex\Vie\Vie2000\Logon","userid","your userid", $true)
$MyWix.AddHKLMRegValue("Software\Securex\Vie\Vie2000\Logon","language","F", $true)
$MyWix.AddHKLMRegValue("Software\Securex\Vie\Vie2000\Logon","password","your password", $true)
$MyWix.AddHKLMRegValue("Software\Securex\Vie\Vie2000\Logon","DB","viep", $true)
$MyWix.AddHKLMRegValue("Software\Securex\Vie\Vie2000\Logon","DBMS","O90", $true)
$MyWix.AddHKLMRegValue("Software\Securex\Vie\Vie2000\Logon","DBPARM","DisableBind=1,CommitOnDisconnect='No',DecimalSeparator=','", $true)
$MyWix.AddHKLMRegValue("Software\Securex\Vie\Vie2000\Logon","APPLICATION","VIE", $true)
$MyWix.AddHKLMRegValue("Software\Securex\Vie\Vie2000\Logon","AUTOLOGON","false", $true)
$MyWix.AddHKLMRegValue("Software\Securex\Vie\Vie2000\Logon","SERVERNAME","viep", $true)
$MyWix.AddHKLMRegValue("Software\Securex\Vie\Vie2000\Logon","DEBUG","N", $true)
$MyWix.AddHKLMRegValue("Software\Securex\Vie\Vie2000\Logon","VERSION","1", $true)
$MyWix.AddHKLMRegValue("Software\Oracle","NLS_LANG","AMERICAN_AMERICA.WE8ISO8859P1", $true)
$MyWix.AddHKLMRegValue("Software\Wow6432Node\Oracle","NLS_LANG","AMERICAN_AMERICA.WE8ISO8859P1", $true)

#Add Desktop Shortcuts
$MyWix.AddDesktopShortcut("VIE2000","$InstallDir\vie2000.exe","/ini=[CommonAppDataFolder]Securex\VIE2000\vie2000.ini",$InstallDir)
$MyWix.AddDesktopShortcut("VIE2000 v2","$InstallDir\vie2000.exe","/ini=[CommonAppDataFolder]Securex\VIE2000\vie2000.ini",$InstallDir)

#Add Startmenu Shortcuts
$MyWix.AddStartMenuShortcut("VIE2000","$InstallDir\vie2000.exe","/ini=[CommonAppDataFolder]Securex\VIE2000\vie2000.ini",$InstallDir,"Securex")
$MyWix.AddStartMenuShortcut("VIE2000 DEV","$InstallDir\vie2000.exe","/DEVELOP /ini=[CommonAppDataFolder]Securex\VIE2000\vie2000.ini",$InstallDir,"Securex")

#Add Environment Variables
$MyWix.AddEnvironmentvariable("PATH","C:\Temp")

#Build WXS File
$MyWix.buildMSIWXS($WixToolsetLocation,"C:\Temp\Test.wxs",$True)

#Generate MSI File
Get-WixMSI -MSIFile "C:\Temp\Test.msi" -WIXToolset $WixToolsetLocation -WXSFile C:\Temp\Test.wxs -DisplayOutput $True