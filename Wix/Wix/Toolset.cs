﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WixSharp;
using WixSharp.CommonTasks;

namespace Wix
{
    public class Toolset
    {
        #region Declarations
        //Wix Project
        private Project _MSIFile = new Project();

        //MSI General Information
        private string _ProductName = string.Empty;
        private string _CompanyName = "Securex";
        private string _ProductVersion = string.Empty;
        private Guid _ProductCode = new Guid();
        private Guid _UpgradeCode = new Guid();

        private string _arpComment = string.Empty;
        private string _arpContact = string.Empty;
        private string _arpManufacturer = string.Empty;
        private bool _arpNoModify = true;
        private bool _arpNoRepair = false;
        private bool _arpNoRemove = false;
        private bool _arpSystemComponent = false;


        private bool _blnAddFiles = false;
        private bool _blnAddProperties = false;
        private bool _blnAddRegistry = false;
        private bool _blnAddDesktopShortcut = false;
        private bool _blnAddStartmenuShortcut = false;



        //General Variables
        private string _SourcePath = string.Empty;
        private string _InstallDir = string.Empty;

        //Shortcut Variables
        private Dir _DesktopFolder = new Dir(@"%Desktop%");
        private Dir _StartMenuFolder = new Dir(@"%ProgramMenu%");

        //Property Variables
        private Property[] _Properties;

        //Registry Variables
        private RegValue[] _Registry;

        //Logging Variables
        private bool _ErrorLoggingOn = false;
        private int _LogLevel = 3; //Error=1 Info=2 Debug=3
        private string _ErrorLogFile = @"C:\Temp\WixError.log";
        #endregion

        #region System Void
        public Toolset() { }
        #endregion

        #region Properties Read / Write
        #region Product Information
        public string productname
        {
            set
            {
                _ProductName = value;
                LogFile("Setting Productname: " + value, "Debug", "productname", 0, 3);
                _MSIFile.Name = _ProductName;
            }
            get
            {
                return _ProductName;
            }
        }
        public string companyname
        {
            set
            {
                _CompanyName = value;
                LogFile("Setting company name: " + value, "Debug", "companyname", 0, 3);
                _MSIFile.ControlPanelInfo.Manufacturer = _CompanyName;
            }

            get
            {
                return _CompanyName;
            }
        }
        public string productversion
        {
            set
            {
                _ProductVersion = value;
                LogFile("Setting Product Version: " + value, "Debug", "productversion", 0, 3);
                _MSIFile.Version = new Version(_ProductVersion);
            }
            get
            {
                return _ProductVersion;
            }
        }
        public Guid productcode
        {
            set
            {
                _ProductCode = value;
                LogFile("Setting Product Code: " + value.ToString(), "Debug", "productcode", 0, 3);
                _MSIFile.ProductId = _ProductCode;
            }
            get
            {
                return _ProductCode;
            }
        }
        public Guid upgradecode
        {
            set
            {
                _UpgradeCode = value;
                LogFile("Setting Upgrade Code: " + value.ToString(), "Debug", "upgradecode", 0, 3);
                _MSIFile.UpgradeCode = _UpgradeCode;
            }
            get
            {
                return _UpgradeCode;
            }
        }

        public string arpcomment
        {
            get { return _arpComment; }
            set 
            { 
                _arpComment = value;
                LogFile("Setting ARP Comment: " + value, "Debug", "arpcomment", 0, 3);
                _MSIFile.ControlPanelInfo.Comments = _arpComment;
            }
        }
        public string arpcontact
        {
            get { return _arpContact; }
            set 
            { 
                _arpContact = value;
                LogFile("Setting ARP Contact: " + value, "Debug", "arpcontact", 0, 3);
                _MSIFile.ControlPanelInfo.Contact = _arpContact;
            }
        }
        public string arpmanufacturer
        {
            get { return _arpManufacturer; }
            set 
            { 
                _arpManufacturer = value;
                LogFile("Setting ARP Manufacturer: " + value, "Debug", "arpmanufacturer", 0, 3);
                _MSIFile.ControlPanelInfo.Manufacturer = _arpManufacturer;
            }
        }
        public bool arpnomodify
        {
            get { return _arpNoModify; }
            set 
            { 
                _arpNoModify = value;
                LogFile("Setting ARP No Modify: " + value.ToString(), "Debug", "arpnomodify", 0, 3);
                _MSIFile.ControlPanelInfo.NoModify = _arpNoModify;
            }
        }
        public bool arpnorepair
        {
            get { return _arpNoRepair; }
            set 
            { 
                _arpNoRepair = value;
                LogFile("Setting ARP No Repair: " + value.ToString(), "Debug", "arpnorepair", 0, 3);
                _MSIFile.ControlPanelInfo.NoRepair = _arpNoRepair;
            }
        }
        public bool arpnoremove
        {
            get { return _arpNoRepair; }
            set 
            { 
                _arpNoRemove = value;
                LogFile("Setting ARP No Remove: " + value.ToString(), "Debug", "arpnoremove", 0, 3);
                _MSIFile.ControlPanelInfo.NoRemove = _arpNoRemove;
            }
        }
        public bool arpsystemcomponent
        {
            get { return _arpSystemComponent; }
            set 
            { 
                _arpSystemComponent = value;
                LogFile("Setting ARP system component: " + value.ToString(), "Debug", "arpsystemcomponent", 0, 3);
                _MSIFile.ControlPanelInfo.SystemComponent = _arpSystemComponent;
            }
        }
        #endregion
        #region MSI Source Information
        public string installdir
        {
            get { return _InstallDir; }
            set 
            { 
                _InstallDir = value;
                LogFile("Setting Installdir: " + value, "Debug", "installdir", 0, 3);

            }
        }
        #endregion
        #region Logging Information
        public bool loggingon
        {
            get { return _ErrorLoggingOn; }
            set { _ErrorLoggingOn = value; }
        }
        public string logfilefullname
        {
            get { return _ErrorLogFile; }
            set { _ErrorLogFile = value; }
        }
        public int loglevel
        {
            get { return _LogLevel; }
            set { _LogLevel = value; }
        }
        #endregion


        #endregion

        #region Properties Read-Only
        public Property[] properties
        {
            get
            {
                return _Properties;
            }
        }
        public RegValue[] registry
        {
            get
            {
                return _Registry;
            }
        }
       
        #endregion

        #region voids
        #region Public
        public void AddEnvironmentvariable(string VariableName, string VariableValue, bool SystemEnv = false)
        {
            try
            {
                LogFile("Adding Environment variable: " + VariableName + " = " + VariableValue + " System: " + SystemEnv.ToString(), "Debug", "AddEnvironmentvariable", 0, 3);
                _MSIFile.AddEnvironmentVariabl(new EnvironmentVariable(VariableName, VariableValue) { Part = EnvVarPart.last, System = SystemEnv });
            }
            catch(Exception ex)
            {
                LogFile(ex.Message, "Error", "AddEnvironmentvariable", ex.LineNumber(), 1);
            }
            
        }
        
        public void AddProperty(string propertyname, string propertyvalue)
        {
            try
            {
                LogFile("Adding Property: " + propertyname + " = " + propertyvalue, "Debug", "AddProperty", 0, 3);
                _Properties = _Properties.AddToArray(new Property(propertyname, propertyvalue));
                _blnAddProperties = true;
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "AddProperty", ex.LineNumber(), 1);
            }
            
        }
        
        public void AddHKLMRegValue(string RegistryKey, string RegistryName, object RegistryValue, bool bln64Bit = false)
        {
            try
            {
                LogFile("Adding HKLM Registry key to " + RegistryKey + " Name: " + RegistryName + " = " + RegistryValue, "Debug", "AddHKLMRegValue", 0, 3);
                if(bln64Bit){
                    RegValue _64BitRegValue = new RegValue(Microsoft.Win32.RegistryHive.LocalMachine, RegistryKey, RegistryName, RegistryValue);
                    _64BitRegValue.Win64 = true;
                    _Registry = _Registry.AddToArray(_64BitRegValue);
                }else{
                    _Registry = _Registry.AddToArray(new RegValue(Microsoft.Win32.RegistryHive.LocalMachine, RegistryKey, RegistryName, RegistryValue));
                }
                _blnAddRegistry = true;
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "AddHKLMRegValue", ex.LineNumber(), 1);
            }
        }
        public void AddHKCURegValue(string RegistryKey, string RegistryName, object RegistryValue)
        {
            try
            {
                LogFile("Adding HKCU Registry key to " + RegistryKey + " Name: " + RegistryName + " = " + RegistryValue, "Debug", "AddHKCURegValue", 0, 3);
                _Registry = _Registry.AddToArray(new RegValue(Microsoft.Win32.RegistryHive.CurrentUser, RegistryKey, RegistryName, RegistryValue));
                _blnAddRegistry = true;
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "AddHKCURegValue", ex.LineNumber(), 1);
            }
        }
        public void AddHKCRRegvalue(string RegistryKey, string RegistryName, object RegistryValue)
        {
            try
            {
                LogFile("Adding HKCR Registry key to " + RegistryKey + " Name: " + RegistryName + " = " + RegistryValue, "Debug", "AddHKCRRegValue", 0, 3);
                _Registry = _Registry.AddToArray(new RegValue(Microsoft.Win32.RegistryHive.ClassesRoot, RegistryKey, RegistryName, RegistryValue));
                _blnAddRegistry = true;
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "AddHKCRRegvalue", ex.LineNumber(), 1);
            }            
        }
        public void AddHKURegValue(string RegistryKey, string RegistryName, object RegistryValue)
        {
            try
            {
                LogFile("Adding HKU Registry key to " + RegistryKey + " Name: " + RegistryName + " = " + RegistryValue, "Debug", "AddHKURegValue", 0, 3);
                _Registry = _Registry.AddToArray(new RegValue(Microsoft.Win32.RegistryHive.Users, RegistryKey, RegistryName, RegistryValue));
                _blnAddRegistry = true;
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "AddHKURegValue", ex.LineNumber(), 1);
            }            
        }
        public void AddHKCCRegValue(string RegistryKey, string RegistryName, object RegistryValue)
        {
            try
            {
                LogFile("Adding HKCC Registry key to " + RegistryKey + " Name: " + RegistryName + " = " + RegistryValue, "Debug", "AddHKCCRegValue", 0, 3);
                _Registry = _Registry.AddToArray(new RegValue(Microsoft.Win32.RegistryHive.CurrentConfig, RegistryKey, RegistryName, RegistryValue));
                _blnAddRegistry = true;
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "AddHKCCRegValue", ex.LineNumber(), 1);
            }            
        }
        
        public void AddSourceFiles(string FilePath)
        {
            LogFile("Adding files from " + FilePath, "Debug", "AddSourceFiles", 0, 3);
            Dir RootFolder = new Dir("[TARGETDIR]");

            System.IO.DirectoryInfo rootfolder = new System.IO.DirectoryInfo(FilePath);
            System.IO.DirectoryInfo[] subDirs = null;

            // Add files in current folder
            RootFolder.AddDirFileCollection(new DirFiles(rootfolder.FullName + "\\*.*"));

            try
            {
                subDirs = rootfolder.GetDirectories();

                foreach (System.IO.DirectoryInfo dirInfo in subDirs)
                {
                    RootFolder.AddDir (ScanSubFolder(dirInfo));
                }
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "AddSourceFiles", ex.LineNumber(),1);
            }

            _MSIFile.AddDir(RootFolder);
            _MSIFile.ResolveWildCards();
            _blnAddFiles = true;
        }
        
        public void AddDesktopShortcut(string DisplayName, string Target, string Arguments, string Workingdir)
        {
            try
            {
                LogFile("Adding Desktop Shortcut Display:" + DisplayName + " Target: " + Target + " Arguments: " + Arguments + " Workingdir: " + Workingdir, "Debug", "AddDesktopShortcut", 0, 3);
                _DesktopFolder.AddShortcut(
                    new ExeFileShortcut(DisplayName, Target, Arguments)
                    {
                        WorkingDirectory = Workingdir
                    });
                _blnAddDesktopShortcut = true;
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "AddDesktopShortcut", ex.LineNumber(), 1);
            } 
            
        }
        public void AddStartMenuShortcut(string DisplayName,string Target,string Arguments,string Workingdir,string Location)
        {
            try
            {
                LogFile("Adding Startmenu Shortcut Display:" + DisplayName + " Target: " + Target + " Arguments: " + Arguments + " Workingdir: " + Workingdir, "Debug", "AddStartMenuShortcut", 0, 3);
                //Check if Folder is already Present
                bool neednew = true;
                foreach (Dir SubDir in _StartMenuFolder.Dirs)
                {
                    if (SubDir.Name == Location)
                    {
                        SubDir.AddShortcut(
                            new ExeFileShortcut(DisplayName, Target, Arguments)
                            {
                                WorkingDirectory = Workingdir
                            });
                        LogFile("Startmenu location already exists. Adding current shortcut", "AddStartMenuShortcut", "addstartmenushortcut", 0, 3);
                        neednew = false;
                    }
                }

                //Folder does not exist
                //Creating a new folder
                if (neednew)
                {
                    LogFile("Startmenu location not found. Creating: " + Location, "AddStartMenuShortcut", "addstartmenushortcut", 0, 3);
                    _StartMenuFolder.AddDir(
                    new Dir(Location,
                        new ExeFileShortcut(DisplayName, Target, Arguments)
                        {
                            WorkingDirectory = Workingdir
                        }));
                }
                _blnAddStartmenuShortcut = true;
            }
            catch(Exception ex)
            {
                LogFile(ex.Message, "Error", "AddStartMenuShortcut", ex.LineNumber(), 1);
            }
        }

        public void buildMSIWXS(string sWixLocation = @"C:\Temp", string WXSFullPath = @"C:\Temp\PSWix.wxs", bool KeepTempFiles = false, string outputfolder = @"C:\Temp")
        {
            LogFile("Checking WXS Folder", "Debug", "buildmsiwxs", 0, 2);
            string sCorrectLocation = string.Empty;
            // Check if WXS Folder exist if not create it.
            try
            {
                string sDirectory = System.IO.Directory.GetParent(WXSFullPath).ToString();
                if (!System.IO.Directory.Exists(sDirectory)) { System.IO.Directory.CreateDirectory(sDirectory); }
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "CreateWXSFolder", ex.LineNumber(), 1);
            }
            LogFile("Checking Output Folder", "Debug", "buildmsiwxs", 0, 2);
            //Check if working folder exists if not create it.
            try
            {
                if (!System.IO.Directory.Exists(outputfolder)) { System.IO.Directory.CreateDirectory(outputfolder); }
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "CreateOutputFolder", ex.LineNumber(), 1);
            }

            //Check the wix toolset folder
            LogFile("Checking WixToolset Folder", "Debug", "buildmsiwxs", 0, 2);
            try
            {
                string sCandleFile = sWixLocation + @"\candle.exe";
                
                if (System.IO.File.Exists(sCandleFile))
                {
                    sCorrectLocation = sWixLocation;
                }
                else
                {
                    sCorrectLocation = sWixLocation + "\bin";
                }
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "CheckWixToolsetFolder", ex.LineNumber(), 1);
            }

            LogFile("Settting Progress UI", "Debug", "buildmsiwxs", 0, 2);
            try
            {
                //Set UI as progress only
                _MSIFile.UI = WUI.WixUI_ProgressOnly;
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "SetProgressUI", ex.LineNumber(), 1);
            }
            LogFile("Add MSI Properties", "Debug", "buildmsiwxs", 0, 2);
            try
            {
                //Add properties to the MSI
                if(_blnAddProperties)
                {
                    _MSIFile.Properties = _Properties;
                }
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "AddProperties", ex.LineNumber(), 1);
            }
            LogFile("Adding MSI Properties", "Debug", "buildmsiwxs", 0, 2);
            try
            {
                //Add Registry entries to the MSI
                if(_blnAddRegistry)
                {
                    _MSIFile.RegValues = _Registry;
                }
                
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "AddRegistry", ex.LineNumber(), 1);
            }
            LogFile("Add Desktop Shortcuts", "Debug", "buildmsiwxs", 0, 2);
            try
            {
                //Add Desktop Shortcuts
                if(_blnAddDesktopShortcut)
                {
                    _MSIFile.AddDir(_DesktopFolder);
                }
                
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "DesktopShortcuts", ex.LineNumber(), 1);
            }
            LogFile("Adding Startmenu shortcuts", "Debug", "buildmsiwxs", 0, 2);
            try
            {
                //Add Startmenu shortcuts
                if(_blnAddStartmenuShortcut)
                {
                    _MSIFile.AddDir(_StartMenuFolder);
                }
                
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "StartMenuShortcuts", ex.LineNumber(), 1);
            }

            LogFile("Adding Upgrade Items", "Debug", "buildmsiwxs", 0, 2);
            try
            {
                //Add Major Upgrade Support
                _MSIFile.MajorUpgradeStrategy = MajorUpgradeStrategy.Default;
                _MSIFile.MajorUpgradeStrategy.RemoveExistingProductAfter = Step.InstallInitialize;
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "SetMajorUpgradeItem", ex.LineNumber(), 1);
            }
            //    LogFile("Setting Output Folder", "Debug", "buildmsiwxs", 0, 2);
            //try
            //{
            //    //Set Output Folder
            //    _MSIFile.OutDir = outputfolder;
            //}
            //catch(Exception ex)
            //{
            //    LogFile(ex.Message, "Error", "SetOutputDir", ex.LineNumber(), 1);
            //}
            LogFile("Creating WXS File", "Debug", "buildmsiwxs", 0, 2);
            try
            {
                LogFile("Wix Location: " + sWixLocation, "Debug", "buildmsiwxs", 0, 2);
                LogFile("WXS File: " + WXSFullPath, "Debug", "buildmsiwxs", 0, 2);
                Compiler.WixLocation = sWixLocation;
                Compiler.PreserveTempFiles = KeepTempFiles;
                Compiler.BuildWxs(_MSIFile, WXSFullPath, Compiler.OutputType.MSI);
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Error", "buildMSIWXS", ex.LineNumber(), 1);
            }

        }
        #endregion
        #region Private
        private void LogFile(string sMessage, string sEventType, string sModuleName, int nErrorLineNo, int nLogLevel)
        {
            if(_ErrorLoggingOn && (nLogLevel <= _LogLevel))
            {
                System.IO.StreamWriter log;
                if (!System.IO.File.Exists(_ErrorLogFile))
                {
                    log = new System.IO.StreamWriter(_ErrorLogFile);
                    log.WriteLine("Date Time;Event Type;Message;Module Name;Error Line no");
                }
                else
                {
                    log = System.IO.File.AppendText(_ErrorLogFile);
                }

                // Write to the file:
                log.WriteLine(DateTime.Now + ";" + sEventType + ";" + sMessage + ";" + sModuleName + ";" + nErrorLineNo);
                // Close the stream:
                log.Close();
            }
        }
        #endregion
        #endregion

        #region functions
        #region Public

        #endregion
        #region Private
        private Id GenerateRandomID()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            string ToReturn = "_" + finalString.ToString();
            return new Id(ToReturn);
        }
        private Dir ScanSubFolder(System.IO.DirectoryInfo sourceFolder)
        {
            Dir WixDir = new Dir(sourceFolder.Name);

            try
            {
                System.IO.DirectoryInfo[] subdirs = null;
                LogFile("Add source files from: " + sourceFolder.FullName, "Debug", "ScanSubFolder", 0, 3);
                //Add files in current folder
                WixDir.AddDirFileCollection(new DirFiles(sourceFolder.FullName + "\\*.*"));

                subdirs = sourceFolder.GetDirectories();

                foreach (System.IO.DirectoryInfo dirInfo in subdirs)
                {
                    WixDir.AddDir(ScanSubFolder(dirInfo));
                }
            }
            catch (Exception ex)
            {
                LogFile(ex.Message, "Debug", "ScanSubFolder", ex.LineNumber(), 1);
            }

            return WixDir;
        }
        private string KnownFolder(string Input)
        {
            List<String> _KnownFolders = new List<string>{ "INSTALLDIR", "AdminToolsFolder", "AppDataFolder", "CommonAppDataFolder", "CommonFilesFolder", "CommonFiles64Folder", "CommonFiles6432Folder", "DesktopFolder", "FavoritesFolder", "FontsFolder", "LocalAppDataFolder", "MyPicturesFolder", "PersonalFolder", "ProgramFilesFolder", "ProgramFiles64Folder", "ProgramFiles6432Folder", "ProgramMenuFolder", "SendToFolder", "StartMenuFolder", "StartupFolder", "SystemFolder", "TempFolder", "TemplateFolder", "WindowsFolder", "WindowsVolume" };
            if(_KnownFolders.Contains(Input))
            {
                return "%" + Input + "%";
            }
            else
            {
                return Input;
            }
        }
        #endregion
        #endregion
    }

    #region Extra Classes
    public static class CollectionHelper
    {
        public static IEnumerable<T> Add<T>(this IEnumerable<T> sequence, T item)
        {
            return (sequence ?? Enumerable.Empty<T>()).Concat(new[] { item });
        }

        public static T[] AddRangeToArray<T>(this T[] sequence, T[] items)
        {
            return (sequence ?? Enumerable.Empty<T>()).Concat(items).ToArray();
        }

        public static T[] AddToArray<T>(this T[] sequence, T item)
        {
            return Add(sequence, item).ToArray();
        }

    }
    public static class ExceptionHelper
    {
        public static int LineNumber(this Exception e)
        {
            int linenum = 0;
            try
            {
                linenum = Convert.ToInt32(e.StackTrace.Substring(e.StackTrace.LastIndexOf(":line") + 5));
            }
            catch
            {
                //Stack trace is not available!
            }
            return linenum;
        }
    }
    #endregion
}
